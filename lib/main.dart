import 'dart:developer';

import 'package:comment_viewer/core/model/Connection.dart';
import 'package:comment_viewer/core/provider/ConnectionStore.dart';
import 'package:comment_viewer/ui/widgets/ConnectionDialog.dart';
import 'package:comment_viewer/ui/widgets/ConnectionList.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<ConnectionStore>(
      create: (_) => ConnectionStore()..getConnections(),
      child: MaterialApp(
        title: 'commentViewer',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: Consumer<ConnectionStore>(
            builder: (context, connectionStore, child) {
          return Scaffold(
            appBar: AppBar(
              title: Text("commentViewer"),
            ),
            body: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: ConnectionList(),
                  ),
                ],
              ),
            ),
            floatingActionButton: FloatingActionButton(
              onPressed: () async {
                log('pressed add');
                String result = await showDialog(
                  context: context,
                  builder: (BuildContext context) =>
                      ConnectionDialog(title: '接続を追加', positiveText: '追加'),
                );
                if (result != null && result.isNotEmpty) {
                  connectionStore.insertConnection(Connection(uri: result));
                }
              },
              child: Icon(Icons.add),
            ),
          );
        }),
      ),
    );
  }
}
