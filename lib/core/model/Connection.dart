class Connection {
  final int id;
  final String uri;
  static final String colId = 'id';
  static final String colUri = 'uri';

  Connection({this.id, this.uri});

  Map<String, dynamic> toMap() {
    return {colId: id, colUri: uri};
  }

  @override
  String toString() {
    return '{ id: $id, uri: $uri }';
  }
}
