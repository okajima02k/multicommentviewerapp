import 'dart:developer';

import 'package:comment_viewer/core/model/Connection.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite_common/sqlite_api.dart';

class ConnectionProvider {
  final String dbName = 'connection';
  static ConnectionProvider _instance;

  factory ConnectionProvider() {
    if (_instance == null) {
      _instance = ConnectionProvider._internal();
    }
    return _instance;
  }

  ConnectionProvider._internal();

  Future<Database> get database async {
    final Future<Database> _database = openDatabase(
      join(await getDatabasesPath(), 'connection_database.db'),
      onCreate: (db, version) {
        return db.execute(
            "CREATE TABLE connection(id INTEGER PRIMARY KEY AUTOINCREMENT, uri TEXT)");
      },
      version: 1,
    );
    return _database;
  }

  Future<void> insertConnection(Connection connection) async {
    final Database db = await database;
    await db.insert(dbName, connection.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace);
    log("inserted ${connection.uri}");
  }

  Future<Connection> getConnection(int id) async {
    final Database db = await database;
    final List<Map<String, dynamic>> maps =
        await db.query(dbName, where: 'id = ?', whereArgs: [id]);

    assert(maps.length > 1, "複数のConnectionを取得しました id: $id");

    return Connection(
        id: maps[0][Connection.colId], uri: maps[0][Connection.colUri]);
  }

  Future<List<Connection>> getConnections() async {
    final Database db = await database;
    final List<Map<String, dynamic>> maps = await db.query(dbName);
    return List.generate(
        maps.length,
        (index) => Connection(
            id: maps[index][Connection.colId],
            uri: maps[index][Connection.colUri]));
  }

  Future<void> updateConnection(Connection connection) async {
    final db = await database;
    await db.update(dbName, connection.toMap(),
        where: "id = ?",
        whereArgs: [connection.id],
        conflictAlgorithm: ConflictAlgorithm.fail);
  }

  Future<void> deleteConnection(int id) async {
    final db = await database;
    await db.delete(dbName, where: "id = ?", whereArgs: [id]);
  }
}
