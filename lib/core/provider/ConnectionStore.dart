import 'dart:developer';

import 'package:comment_viewer/core/infrastructure/ConnectionProvider.dart';
import 'package:comment_viewer/core/model/Connection.dart';
import 'package:flutter/cupertino.dart';

class ConnectionStore with ChangeNotifier {
  List<Connection> list = [];
  final ConnectionProvider db = ConnectionProvider();

  void getConnections() async {
    list = await db.getConnections();
    notifyListeners();
    log('notify getConnections: get ${list.length} items');
  }

  void insertConnection(Connection connection) async {
    await db.insertConnection(connection);
    getConnections();
    notifyListeners();
    log('notify insertConnection: ' + connection.toString());
  }

  void updateConnection(Connection connection) async {
    await db.updateConnection(connection);
    getConnections();
    notifyListeners();
    log('notify updateConnection: ' + connection.toString());
  }

  void deleteConnection(int id) async {
    await db.deleteConnection(id);
    getConnections();
    notifyListeners();
    log('notify deleteConnection: ' + id.toString());
  }
}
