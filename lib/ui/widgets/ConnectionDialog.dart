import 'package:flutter/material.dart';

class ConnectionDialog extends StatefulWidget {
  final String title;
  final String positiveText;
  final String uri;

  ConnectionDialog(
      {@required this.title, @required this.positiveText, this.uri});

  @override
  State<StatefulWidget> createState() => _AddConnectionState();
}

class _AddConnectionState extends State<ConnectionDialog> {
  TextEditingController _textEditingController;

  @override
  void initState() {
    super.initState();
    _textEditingController =
        TextEditingController(text: widget.uri ?? widget.uri);
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(widget.title),
      content: TextField(
        controller: _textEditingController,
        decoration: InputDecoration(
          labelText: '接続先URL',
          hintText: 'https://www.twitch.tv/xxx',
        ),
      ),
      actions: <Widget>[
        TextButton(
          onPressed: () => Navigator.of(context).pop(""),
          child: Text('キャンセル'),
        ),
        TextButton(
          onPressed: () =>
              Navigator.of(context).pop(_textEditingController.text),
          child: Text(widget.positiveText),
        ),
      ],
    );
  }
}
