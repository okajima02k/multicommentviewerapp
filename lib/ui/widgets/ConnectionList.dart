import 'dart:developer';

import 'package:comment_viewer/core/model/Connection.dart';
import 'package:comment_viewer/core/provider/ConnectionStore.dart';
import 'package:comment_viewer/ui/widgets/ConnectionDialog.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ConnectionList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<ConnectionStore>(
      create: (_) => ConnectionStore()..getConnections(),
      child:
          Consumer<ConnectionStore>(builder: (context, connectionStore, child) {
        final List<Connection> connections = connectionStore.list;
        return ListView(
          children: connections
                  ?.map(
                    (connection) => Card(
                      child: ListTile(
                        title: RichText(
                          text: TextSpan(
                            text: connection.uri,
                            style: TextStyle(color: Colors.black87),
                          ),
                        ),
                        trailing: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            IconButton(
                              icon: Icon(Icons.edit),
                              onPressed: () async {
                                log('pressed edit: ' + connection.toString());
                                String result = await showDialog(
                                  context: context,
                                  builder: (BuildContext context) =>
                                      ConnectionDialog(
                                    title: '接続を編集',
                                    positiveText: '更新',
                                    uri: connection.uri,
                                  ),
                                );
                                if (result != null && result.isNotEmpty) {
                                  final Connection updateConnection =
                                      Connection(
                                    id: connection.id,
                                    uri: result,
                                  );
                                  connectionStore
                                      .updateConnection(updateConnection);
                                }
                              },
                            ),
                            IconButton(
                              icon: Icon(Icons.delete),
                              onPressed: () {
                                log('pressed delete');
                              },
                            )
                          ],
                        ),
                      ),
                    ),
                  )
                  ?.toList() ??
              [
                ListTile(
                  title: Text(""),
                )
              ],
        );
      }),
    );
  }
}
